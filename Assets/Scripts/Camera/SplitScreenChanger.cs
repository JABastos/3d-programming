﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SplitScreenChanger : MonoBehaviour {

    public Camera cameraP1;
    public Camera cameraP2;

    public bool Horizontal = false;
	
	// Update is called once per frame
	void Update () {

        if (Input.GetKeyDown(KeyCode.Space))
            ChangeSplitScreen();
	}

    public void ChangeSplitScreen()
    {
        Horizontal = !Horizontal;

        if(Horizontal)
        {
            cameraP1.rect = new Rect(0, 0.5f, 1, 0.5f);
            cameraP2.rect = new Rect(0, 0, 1, 0.5f);
        }
        else
        {
            cameraP1.rect = new Rect(0, 0, 0.5f, 1);
            cameraP2.rect = new Rect(0.5f, 0, 0.5f, 1);
        }
    }
}
