﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankPowerUp : MonoBehaviour
{
    public void ActivatePowerUpDamage(Collider player, float powerUpEffect, Color shellColor)
    {
        player.GetComponent<TankShooting>().m_ExplosionRadius = powerUpEffect;
        player.GetComponent<TankShooting>().m_shellColor = shellColor;
        player.GetComponent<TankShooting>().m_Shell.GetComponent<Light>().color = shellColor;
    }

    public void DeactivatePowerUpDamage(Collider player, float powerUpDefault, Color shellColor)
    {
        player.GetComponent<TankShooting>().m_ExplosionRadius = powerUpDefault;
        player.GetComponent<TankShooting>().m_shellColor = shellColor;
        player.GetComponent<TankShooting>().m_Shell.GetComponent<Light>().color = Color.white;
    }

    public void ActivatePowerUpHeal(Collider player, float powerUpEffect)
    {
        player.GetComponent<TankHealth>().TakeDamage(-powerUpEffect);
    }
}
