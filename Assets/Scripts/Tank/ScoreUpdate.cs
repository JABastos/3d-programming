﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreUpdate : MonoBehaviour {
    [HideInInspector] public Text UIPanel;

    [HideInInspector] public int score = 0;

    private int player;
	// Use this for initialization
	void Start () {
        int player = gameObject.GetComponent<TankShooting>().m_PlayerNumber;

        if(player == 1)
        {
            UIPanel = GameObject.Find("TextP2").GetComponent<Text>();
        }
        else
        {
            UIPanel = GameObject.Find("TextP1").GetComponent<Text>();
        }
    }
	
	// Update is called once per frame
	void Update () {
            UIPanel.text = "SCORE: " + score;
    }
}
