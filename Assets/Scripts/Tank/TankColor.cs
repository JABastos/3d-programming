﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankColor : MonoBehaviour {

	public void ChangeColor(Color color)
    {
        MeshRenderer[] renderers = GetComponentsInChildren<MeshRenderer>();

        for (int i = 0; i < renderers.Length; i++)
        {
            renderers[i].material.color = color;
        }
    }
}
