﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShellRotation : MonoBehaviour {

    public Rigidbody m_Shell;
    public float m_RotationSpeed = 1;
    public float m_RotationFactor = 0.1f;

    private float m_rotationEnd = 30f;
    private float m_currentRotation = 0f;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        
        if(m_currentRotation < m_rotationEnd)
        {
            m_Shell.transform.Rotate(new Vector3(m_RotationFactor, 0, 0) * m_RotationSpeed);
            m_currentRotation += m_RotationFactor;
        }
    }
}
