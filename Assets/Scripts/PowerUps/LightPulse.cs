﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightPulse : MonoBehaviour {
    public float duration = 1.0F;

    private Light m_light;

    private void Start()
    {
        m_light = GetComponent<Light>();
    }

    // Update is called once per frame
    void Update () {
        float phi = Time.time / duration * 2 * Mathf.PI;
        float amplitude = Mathf.Cos(phi) * 0.5F + 0.5F;
        m_light.intensity = amplitude;
    }
}
