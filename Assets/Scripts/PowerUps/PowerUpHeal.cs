﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUpHeal : MonoBehaviour {

    public GameObject m_PickupEffect;
    public float m_PowerUpValue = 25f;


    private void OnTriggerEnter(Collider other)
    {

        if (other.CompareTag("Player"))
        {
            Pickup(other);
        }
    }

    void Pickup(Collider player)
    {
        //Spawn effect
        Instantiate(m_PickupEffect, transform.position, transform.rotation);

        //Apply effect to player
        player.GetComponent<TankPowerUp>().ActivatePowerUpHeal(player, m_PowerUpValue);

        //Deactivate Mesh and Collider
        GetComponent<MeshRenderer>().enabled = false;
        GetComponent<Collider>().enabled = false;
        GetComponent<Light>().enabled = false;

        //Remove power up object
        Destroy(gameObject);
    }
}
