﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUpSpawnAI: MonoBehaviour
{

    public GameObject m_PickupEffect;
    public GameObject m_Enemy;
    public Transform m_SpawnPoint;
    public float m_ActiveTime = 5f;


    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            StartCoroutine(Pickup(other));
        }
    }

    IEnumerator Pickup(Collider player)
    {
        //Spawn effect
        Instantiate(m_PickupEffect, transform.position, transform.rotation);

        //Apply effect
        GameObject enemyInstance =
            Instantiate(m_Enemy, m_SpawnPoint.position, m_SpawnPoint.rotation) as GameObject;

        GameObject[] Players = GameObject.FindGameObjectsWithTag("Player");
        GameObject EnemyPlayer = null;

        foreach (GameObject p in Players)
        {
            if(p.GetComponent<TankShooting>().m_PlayerNumber != player.GetComponent<TankShooting>().m_PlayerNumber)
            {
                EnemyPlayer = p;   
            }
        }

        enemyInstance.GetComponent<PowerUpAI>().m_target = EnemyPlayer.transform;

        //Deactivate Mesh and Collider
        GetComponent<MeshRenderer>().enabled = false;
        GetComponent<Collider>().enabled = false;
        GetComponent<Light>().enabled = false;

        //Wait until de PowerUp ends
        yield return new WaitForSeconds(m_ActiveTime);

        //Remove power up object
        GameObject[] Projectiles = GameObject.FindGameObjectsWithTag("AIProjectile");
        foreach (GameObject p in Projectiles)
        {
            Destroy(p);
        }

        Destroy(enemyInstance);
        Destroy(gameObject);
    }
}
