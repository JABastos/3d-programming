﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUpDamage : MonoBehaviour {

    public GameObject m_PickupEffect;
    public float m_PowerUpValue = 10f;
    public float m_DefaultValue = 5f;
    public float m_ActiveTime = 2f;
    public Color m_Color;

    private Color m_PlayerColor;
    private Color m_ShellColor;

    private void OnTriggerEnter(Collider other)
    {
        m_PlayerColor = other.GetComponentsInChildren<MeshRenderer>()[0].material.color;
        m_ShellColor = other.GetComponent<TankShooting>().m_shellColor;

        if (other.CompareTag("Player"))
        {
            StartCoroutine(Pickup(other));
        }
    }

    IEnumerator Pickup(Collider player)
    {
        //Spawn effect
        Instantiate(m_PickupEffect, transform.position, transform.rotation);

        //Apply effect to player
        player.GetComponent<TankPowerUp>().ActivatePowerUpDamage(player, m_PowerUpValue, m_Color);
        player.GetComponent<TankColor>().ChangeColor(m_PlayerColor + new Color(0.1f, 0, 0, 0));

        //Deactivate Mesh and Collider
        GetComponent<MeshRenderer>().enabled = false;
        GetComponent<Collider>().enabled = false;
        GetComponent<Light>().enabled = false;

        //Wait until de PowerUp ends
        yield return new WaitForSeconds(m_ActiveTime);
        
        //Return to normal state
        player.GetComponent<TankPowerUp>().DeactivatePowerUpDamage(player, m_DefaultValue, m_ShellColor);
        player.GetComponent<TankColor>().ChangeColor(m_PlayerColor);

        //Remove power up object
        Destroy(gameObject);
    }
}
