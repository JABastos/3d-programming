﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUpAI : MonoBehaviour {

    public Transform m_target;    
    public Rigidbody m_projectile;

    public float m_velocity = 1f;
    public float m_maximumLookDistance = 1000f;
    public float m_maximumAttackDistance = 1000f;
    public float m_rotationDamping = 2f;
 
    public float m_shotInterval = 0.5f;
    private float m_shotTime = 0;
 

    void Update()
    {
        var distance = Vector3.Distance(m_target.position, transform.position);

        if (distance <= m_maximumLookDistance)
        {
            LookAtTarget();

            //Check distance and time
            if (distance <= m_maximumAttackDistance && (Time.time - m_shotTime) > m_shotInterval)
            {
                Shoot();
            }
        }
    }


    void LookAtTarget()
    {
        var dir = m_target.position - transform.position;
        dir.y = 0;
        var rotation = Quaternion.LookRotation(dir);
        transform.rotation = Quaternion.Slerp(transform.rotation, rotation, Time.deltaTime * m_rotationDamping);
    }


    void Shoot()
    {
        //Reset the time when we shoot
        m_shotTime = Time.time;

        Rigidbody projectileInstance =
            Instantiate(m_projectile, transform.position + (m_target.position - transform.position).normalized, Quaternion.LookRotation(m_target.position - transform.position)) as Rigidbody;
        projectileInstance.velocity = m_velocity * (m_target.position - transform.position);

    }
}
