﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterMoving : MonoBehaviour {

    Renderer renderer;
    float offset;

	// Use this for initialization
	void Start () {
        renderer = GetComponent<Renderer>();
	}
	
	// Update is called once per frame
	void Update () {
        offset = Time.time * 0.005f;
        renderer.material.mainTextureOffset = new Vector2(offset, 0);
    }
}
