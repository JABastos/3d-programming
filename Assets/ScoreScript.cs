﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreScript : MonoBehaviour {

	// Use this for initialization
	void Start () {
        int[] scores = Static.CrossSceneInformation;



        GameObject.Find("P1Score").GetComponent<Text>().text = "" + scores[0];
        GameObject.Find("P2Score").GetComponent<Text>().text = "" + scores[1];
    }
}
